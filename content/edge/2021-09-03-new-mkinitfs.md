title: "Important updates to initramfs creation and boot files"
date: 2021-09-03
---

pmbootstrap 1.37.0 and `postmarketos-mkinitfs` >= 1.0.0 will include
significant changes to the way that boot files (kernel, initramfs, boot.img)
are created and installed on postmarketOS:

### Boot files no longer have the "flavor" appended to the file names

The "flavor" is a string that denotes which kernel variant/package the file is
from, and was originally included because there was some idea of supporting
multiple installed kernel variants at once. This feature was never really fully
implemented/supported in pmOS, so it was decided to simplify file naming by
removing it from the file names. As an example,
`/boot/vmlinuz-postmarketos-allwinner` is now named `/boot/vmlinuz`.

### New tool for generating the initramfs/-extra, and auxiliary (boot.img, etc) files
The shell script for finalizing boot files (e.g. creating `boot.img`, appending
dtb to the kernel image, etc) as been split off into a separate
repository/project, to allow other Linux distributions to easily package and
utilize it. The new project is
[boot-deploy](https://gitlab.com/postmarketOS/boot-deploy).

The existing `mkinitfs` tool as been rewritten to support, and together with
`boot-deploy` it supports:

- Verifying the creation of boot files, and installing them atomically to `/boot`
- Faster initramfs/-extras creation

Users on Edge will receive an upgrade starting today for
`postmarketos-mkinitfs` that bring these changes.

While we don't anticipate any issues, and have spent a lot of time testing
these changes, there is a possibility that things may break. If you experience
any problems, please [file an issue on gitlab](https://postmarketos.org/issues),
or [reach out to us on chat](https://wiki.postmarketos.org/wiki/Matrix_and_IRC)

Related:

- [pmb!2093](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2093)
- [pma!2426](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2426)
- [postmarketos-mkinitfs](https://gitlab.com/postmarketOS/postmarketos-mkinitfs)
- [boot-deploy](https://gitlab.com/postmarketOS/boot-deploy)
